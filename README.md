ScalaTerm provides an interface to terminals and terminal emulators. Currently, 
the only built-in `TerminalImplementation` is `TerminalImplementationSwing`, but
others can be implemented relatively simply. Characters are based on Code Page
437, and currently expect an 8x12 tileset containing 256 8x12 tiles (this is 
based on Dwarf Fortress' solution, don't judge me). Colors are not limited to
VGA colors, but Java's `Color` constants replicate VGA colors, so use them if
you want those colors.

Also provides some custom grid-based geometry classes, namely `Point`, `Line`, 
`Dimension`,`Rectangle`, and `Direction`, most of which are based vaguely on 
AWT's classes. These will probably be refactored at some point to use another 
library, or at least to generalize the notion of distance (which is currently 
chessboard distance).