package com.gitlab.scalaterm

import java.awt.event.KeyEvent

import scala.concurrent.Future

trait TerminalImplementation {
	
	protected [scalaterm] def terminalSize: Dimension
	
	protected [scalaterm] def push(buffer: Terminal#Buffer): Unit
	
	protected [scalaterm] def listen(listener: KeyEvent ⇒ Unit): Unit
	
	protected [scalaterm] def nextKey: Future[KeyEvent]
	
	protected [scalaterm] def open(): Unit
	
	protected [scalaterm] def close(): Unit
	
	protected [scalaterm] def isClosed: Boolean

}
