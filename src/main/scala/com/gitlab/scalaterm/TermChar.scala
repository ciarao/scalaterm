package com.gitlab.scalaterm

import java.awt.Color

final case class TermChar(private[scalaterm] val code: Code, private[scalaterm] val fg: Color, private[scalaterm] val bg: Color) {
	
	def darker: TermChar = this.copy(fg = fg.darker(), bg = bg.darker())
	
	def lighter: TermChar = this.copy(fg = fg.brighter(), bg = bg.brighter())
	
}

final case class Code private (innerValue: Short) extends AnyVal

object Code {
	
	private[scalaterm] def apply(value: Short): Code = {
		require(value >= 0 && value <= 255, "Character codes are limited to the range 0-255.")
		new Code(value)
	}
	
	private[scalaterm] def apply(value: Int): Code = {
		require(value >= 0 && value <= 255, "Character codes are limited to the range 0-255.")
		new Code(value.toShort)
	}
	
	def apply(char: Char): Code = {
		require(char >= 0x20 && char <= 0x7E, "Given character does not map well to Code Page 437.")
		Code(char.toShort)
	}
	
	def apply(specialChar: SpecialCharacters.Value): Code = Code(specialChar.id.toShort)
	
	object SpecialCharacters extends Enumeration {
		val Null, Smiley, InvSmiley, Heart, Diamond, Club, Spade, Bullet, InvBullet,
			Circle, InvCircle, Male, Female, SingleNote, DoubleNote, Solar, RightTriangle, LeftTriangle,
			ArrowUpDown, DoubleExclamation, Paragraph, Section, HorizontalLine, ArrowUpDownFloor, ArrowUp,
			ArrowDown, ArrowLeft, ArrowRight, RightAngle, ArrowLeftRight, UpTriangle, DownTriangle = Value
		val CapitalDelta: Value = Value(0x7F)
		val Cent: Value = Value(0x9B)
		val Pound, Yen, Peseta = Value
		val OrdinalFem: Value = Value(0xA6)
		val OrdinalMasc, InvQuestion, InvNot, Not, OneHalf, OneFourth, InvExclamation, LeftGuillemet, RightGuillemet,
			LightShade, MediumShade, DarkShade, BoxLightVertical, BoxLightVerticalLeft, BoxVerticalSingleLeftDouble,
			BoxVerticalDoubleLeftSingle, BoxDownDoubleLeftSingle, BoxDownSingleLeftDouble, BoxDoubleVerticalLeft,
			BoxDoubleVertical, BoxDoubleDownLeft, BoxDoubleUpLeft, BoxUpDoubleLeftSingle, BoxUpSingleLeftDouble,
			BoxLightDownLeft, BoxLightUpRight, BoxLightUpHorizontal, BoxLightDownHorizontal, BoxLightVerticalRight,
			BoxLightHorizontal, BoxLightVerticalHorizontal, BoxVerticalSingleRightDouble, BoxVerticalDoubleRightSingle,
			BoxDoubleUpRight, BoxDoubleDownRight, BoxDoubleUpHorizontal, BoxDoubleDownHorizontal, BoxDoubleVerticalRight,
			BoxDoubleHorizontal, BoxDoubleVerticalHorizontal, BoxUpSingleHorizontalDouble, BoxUpDoubleHorizontalSingle,
			BoxDownSingleHorizontalDouble, BoxDownDoubleHorizontalSingle, BoxUpDoubleRightSingle, BoxUpSingleRightDouble,
			BoxDownSingleRightDouble, BoxDownDoubleRightSingle, BoxVerticalDoubleHorizontalSingle, BoxVerticalSingleHorizontalDouble,
			BoxLightUpLeft, BoxLightDownRight, FullBlock, LowerHalfBlock, LeftHalfBlock, RightHalfBlock, UpperHalfBlock, Alpha,
			Beta, Gamma, Pi, CapitalSigma, LowercaseSigma, Mu, Tau, CapitalPhi, Theta, Omega, LowercaseDelta, Infinity, LowercasePhi, Epsilon,
			Intersection, Identical, PlusMinus, GreaterEqual, LessEqual, IntegralTop, IntegralBottom, Division, ApproxEqual,
			Degree, BulletOperator, MiddleDot, SquareRoot, SuperN, Super2, BlackSquare, NoSquare = Value
	}
	
}