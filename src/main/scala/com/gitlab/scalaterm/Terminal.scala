package com.gitlab.scalaterm

import java.awt.Color
import java.awt.event.KeyEvent

import scala.concurrent.Future

class Terminal(private val impl: TerminalImplementation) {
	
	impl.open()
	
	type Buffer = Array[Array[TermChar]]
	
	private val backBuffer: Buffer = Array.fill(impl.terminalSize.width, impl.terminalSize.height)(TermChar(Code(0), Color.BLACK, Color.BLACK))
	
	def setPoint(x: Int, y: Int, to: TermChar): Unit = backBuffer(x)(y) = to
	def setPoint(point: Point, to: TermChar): Unit = setPoint(point.x, point.y, to)
	
	def setHorizontalLine(x0: Int, x1: Int, y: Int, to: TermChar): Unit = (x0 to x1).foreach(x ⇒ setPoint(x, y, to))
	def setHorizontalLine(x0: Int, x1: Int, y: Int, to: (Int, Int) ⇒ TermChar): Unit = (x0 to x1).foreach(x ⇒ setPoint(x, y, to(x, y)))
	
	def setVerticalLine(x: Int, y0: Int, y1: Int, to: TermChar): Unit = (y0 to y1).foreach(y ⇒ setPoint(x, y, to))
	def setVerticalLine(x: Int, y0: Int, y1: Int, to: (Int, Int) ⇒ TermChar): Unit = (y0 to y1).foreach(y ⇒ setPoint(x, y, to(x, y)))
	
	def setRectangle(x0: Int, x1: Int, y0: Int, y1: Int, to: TermChar): Unit = for (x ← x0 to x1; y ← y0 to y1) { setPoint(x, y, to) }
	def setRectangle(x0: Int, x1: Int, y0: Int, y1: Int, to: (Int, Int) ⇒ TermChar): Unit = for (x ← x0 to x1; y ← y0 to y1) { setPoint(x, y, to(x, y)) }
	def setRectangle(rectangle: Rectangle, to: TermChar): Unit = setRectangle(rectangle.x, rectangle.y,
	                                                                          rectangle.x + rectangle.width, rectangle.y + rectangle.height, to)
	def setRectangle(rectangle: Rectangle, to: (Int, Int) ⇒ TermChar): Unit = setRectangle(rectangle.x, rectangle.y,
	                                                                                       rectangle.x + rectangle.width, rectangle.y + rectangle.height, to)
	
	def clear(): Unit = setRectangle(0, 0, impl.terminalSize.width, impl.terminalSize.height, TermChar(Code(0), Color.BLACK, Color.BLACK))
	
	def push(): Unit = impl.push(backBuffer)
	
	def listen(listener: KeyEvent ⇒ Unit): Unit = impl.listen(listener)
	
	def nextKey: Future[KeyEvent] = impl.nextKey
	
	def close(): Unit = impl.close()
	
	def isClosed: Boolean = impl.isClosed
	
}