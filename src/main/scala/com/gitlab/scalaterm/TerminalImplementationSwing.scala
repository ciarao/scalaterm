package com.gitlab.scalaterm

import java.awt.event.{KeyAdapter, KeyEvent, WindowAdapter, WindowEvent}
import java.awt.image.BufferedImage
import java.awt.{Color, Toolkit}

import javax.swing.{ImageIcon, JFrame, JLabel, WindowConstants}

import scala.concurrent.{Future, Promise}

class TerminalImplementationSwing(val title: String, val charset: Charset, override val terminalSize: Dimension,
                                  scale: SwingScale = SwingScale.automatic()) extends TerminalImplementation {
	
	private val properScale: Int = scale.scale.right.map(defaultScale).merge
	
	private val frame: JFrame = new JFrame(title)
	
	frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
	frame.setBackground(Color.BLACK)
	frame.setResizable(false)
	
	private var keyPromise: Promise[KeyEvent] = Promise()
	
	private val image: BufferedImage = new BufferedImage(terminalSize.width * 8 * properScale, terminalSize.height * 12 * properScale, BufferedImage.TYPE_INT_RGB)
	
	private val label: JLabel = new JLabel(new ImageIcon(image))
	
	frame.add(label)
	frame.pack()
	
	frame.addKeyListener(new KeyAdapter {
		override def keyPressed(e: KeyEvent): Unit = {
			onKeyPress(e)
		}
	})
	
	frame.addWindowListener(new WindowAdapter {
		override def windowClosed(e: WindowEvent): Unit = {
			onClose()
		}
	})
	
	private def onKeyPress(e: KeyEvent): Unit = {
		val oldPromise = keyPromise
		keyPromise = Promise()
		oldPromise.success(e)
	}
	
	private def onClose(): Unit = {
		val oldPromise = keyPromise
		keyPromise = Promise()
		oldPromise.failure(new RuntimeException("Terminal closed"))
	}
	
	private def defaultScale(screenProportion: Double): Int = {
		val screenSize = Toolkit.getDefaultToolkit.getScreenSize
		val widthScale = Math.floorDiv((screenSize.width * screenProportion).toInt, terminalSize.width * 8)
		val heightScale = Math.floorDiv((screenSize.height * screenProportion).toInt, terminalSize.height * 12)
		Math.min(widthScale, heightScale)
	}
	
	protected [scalaterm] override def open(): Unit = {
		frame.setVisible(true)
	}
	
	protected [scalaterm] override def push(buffer: Terminal#Buffer): Unit = {
		val copy = Array.fill(terminalSize.width, terminalSize.height)(TermChar(Code(0), Color.BLACK, Color.BLACK))
		for (x ← 0 until terminalSize.width) {
			for (y ← 0 until terminalSize.height) {
				copy(x)(y) = buffer(x)(y)
			}
		}
		val tempImg = new BufferedImage(terminalSize.width * 8, terminalSize.height * 12, BufferedImage.TYPE_INT_RGB)
		val tg = tempImg.createGraphics()
		for (x ← 0 until terminalSize.width) {
			for (y ← 0 until terminalSize.height) {
				charset.draw(copy(x)(y), tg, x * 8, y * 12)
			}
		}
		tg.dispose()
		val g = image.createGraphics()
		g.drawImage(tempImg, 0, 0, image.getWidth(), image.getHeight(), 0, 0, tempImg.getWidth(), tempImg.getHeight(), null)
		label.setIcon(new ImageIcon(image))
	}
	
	protected [scalaterm] override def listen(listener: KeyEvent ⇒ Unit): Unit = {
		frame.addKeyListener(new KeyAdapter {
			override def keyPressed(e: KeyEvent): Unit = listener(e)
		})
	}
	
	protected [scalaterm] override def nextKey: Future[KeyEvent] = keyPromise.future
	
	protected [scalaterm] override def close(): Unit = {
		frame.setVisible(false)
		frame.dispose()
		onClose()
	}
	
	protected [scalaterm] override def isClosed: Boolean = !frame.isVisible
	
}

final case class SwingScale private (scale: Either[Int, Double]) extends AnyVal

object SwingScale {
	
	def absolute(abs: Int): SwingScale = SwingScale(Left(abs))
	
	def automatic(screenProportion: Double = 0.66): SwingScale = SwingScale(Right(screenProportion))
	
	def apply(abs: Int): SwingScale = absolute(abs)
	
	def apply(screenProportion: Double = 0.66): SwingScale = automatic(screenProportion)
	
}