package com.gitlab.scalaterm

import java.awt.Graphics2D
import java.awt.image.BufferedImage

import scala.util.{Failure, Success, Try}

case class Charset private (private val atlas: Array[AtlasChar]) extends AnyVal {
	
	def draw(character: TermChar, g2: Graphics2D, dx: Int, dy: Int): Unit = {
		g2.setColor(character.bg)
		g2.fillRect(dx, dy, 8, 12)
		g2.setColor(character.fg)
		atlas(character.code.innerValue).draw(g2, dx, dy)
	}

}

object Charset {

	def from(image: BufferedImage): Try[Charset] = {
		if (image.getWidth % 8 != 0 || image.getHeight % 12 != 0) {
			Failure(new IllegalArgumentException("Image size is not divisible by 8x12"))
		} else {
			val tilesWidth = image.getWidth / 8
			val tilesHeight = image.getHeight / 12
			if (tilesWidth * tilesHeight != 256) {
				Failure(new IllegalArgumentException("Image does not contain 256 tiles"))
			} else {
				Success(Charset(Range(0, tilesHeight).flatMap(y ⇒ Range(0, tilesWidth).map(x ⇒ AtlasChar(image, x * 8, y * 12))).toArray))
			}
		}
	}

}

final private[scalaterm] case class AtlasChar private (private val lines: Array[Byte]) extends AnyVal {
	
	def draw(g2: Graphics2D, dx: Int, dy: Int): Unit = {
		for (y ← 0 until 12) {
			val byte = lines(y)
			for (x ← 0 until 8) {
				if (((byte >> x) & 1) == 1) {
					g2.drawRect(x + dx, y + dy, 0, 0)
				}
			}
		}
	}
	
}

private[scalaterm] object AtlasChar {
	
	def apply(image: BufferedImage, sx: Int, sy: Int): AtlasChar = {
		val normalized = new BufferedImage(8, 12, BufferedImage.TYPE_BYTE_GRAY)
		val g2 = normalized.createGraphics
		g2.drawImage(image, 0, 0, 8, 12, sx, sy, sx + 8, sy + 12, null)
		g2.dispose()
		val lines = Array.ofDim[Byte](12)
		for (y ← 0 until 12) {
			var byte: Byte = 0
			for (x ← 0 until 8) {
				if (normalized.getRGB(x, y) == -1) {
					byte = (byte | (1 << x)).toByte
				}
			}
			lines(y) = byte
		}
		AtlasChar(lines)
	}
	
}