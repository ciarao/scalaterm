package com.gitlab.scalaterm

case class Point(x: Int = 0, y: Int = 0) {
	def translate(dx: Int = 0, dy: Int = 0): Point = Point(x + dx, y + dy)
	def distance(other: Point): Int = Math.max(Math.abs(other.x - this.x), Math.abs(other.y - this.y))
	def translate(direction: Direction): Point = translate(direction.x, direction.y)
}

case class Line(x0: Int = 0, y0: Int = 0, x1: Int = 0, y1: Int = 0) {
	def points: (Point, Point) = (Point(x0, y0), Point(x1, y1))
	def length: Int = Math.max(Math.abs(x1 - x0), Math.abs(y1 - y0))
}

case class Dimension(width: Int = 0, height: Int = 0) {
	def scale[T](s: T)(implicit num: Numeric[T]): Dimension = Dimension(num.toInt(num.times(num.fromInt(width), s)),
	                                                                    num.toInt(num.times(num.fromInt(height), s)))
}

case class Rectangle(x: Int = 0, y: Int = 0, width: Int = 0, height: Int = 0) {
	def this(point: Point, dim: Dimension) = this(point.x, point.y, dim.width, dim.height)
	def point: Point = Point(x, y)
	def size: Dimension = Dimension(width, height)
	def translate(dx: Int = 0, dy: Int = 0): Rectangle = Rectangle(x = x + dx, y = y + dy)
	def grow(dw: Int = 0, dh: Int = 0): Rectangle = Rectangle(width = width + dw, height = height + dh)
}

sealed abstract class Direction(private [scalaterm] val x: Int, private [scalaterm] val y: Int)
object Direction {
	val AllDirections: Seq[Direction] = Seq(NorthWest, North, NorthEast, West, Center, East, SouthWest, South, SouthEast)
	val NeighborDirections: Seq[Direction] = Seq(NorthWest, North, NorthEast, West, East, SouthWest, South, SouthEast)
	val OrthogonalDirections: Seq[Direction] = Seq(North, West, East, South)
	val DiagonalDirections: Seq[Direction] = Seq(NorthWest, NorthEast, SouthWest, SouthEast)
	case object NorthWest   extends Direction(-1, -1)
	case object North       extends Direction(0,  -1)
	case object NorthEast   extends Direction(1,  -1)
	case object West        extends Direction(-1,  0)
	case object Center      extends Direction(0,   0)
	case object East        extends Direction(1,   0)
	case object SouthWest   extends Direction(-1,  1)
	case object South       extends Direction(0,   1)
	case object SouthEast   extends Direction(1,   1)
}