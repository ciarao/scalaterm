package com.gitlab.scalaterm

import com.gitlab.scalaterm.Code.SpecialCharacters

final case class BoxSelector private (private val hasUp: Boolean, private val hasDown: Boolean,
                                      private val hasRight: Boolean, private val hasLeft: Boolean) {

	def left: BoxSelector = this.copy(hasLeft = true)
	
	def right: BoxSelector = this.copy(hasRight = true)
	
	def up: BoxSelector = this.copy(hasUp = true)
	
	def down: BoxSelector = this.copy(hasDown = true)
	
	def single: Code = BoxSelector.singleMap((hasUp, hasDown, hasRight, hasLeft))
	
	def double: Code = BoxSelector.doubleMap((hasUp, hasDown, hasRight, hasLeft))

}

object BoxSelector {
	
	private val singleMap: Map[(Boolean, Boolean, Boolean, Boolean), Code] = Map(
		(false, false, false, false) → Code('O'),
		(false, false, false, true) → Code('O'),
		(false, false, true, false) → Code('O'),
		(false, false, true, true) → Code(SpecialCharacters.BoxLightHorizontal),
		(false, true, false, false) → Code('O'),
		(false, true, false, true) → Code(SpecialCharacters.BoxLightDownLeft),
		(false, true, true, false) → Code(SpecialCharacters.BoxLightDownRight),
		(false, true, true, true) → Code(SpecialCharacters.BoxLightDownHorizontal),
		(true, false, false, false) → Code('O'),
		(true, false, false, true) → Code(SpecialCharacters.BoxLightUpLeft),
		(true, false, true, false) → Code(SpecialCharacters.BoxLightUpRight),
		(true, false, true, true) → Code(SpecialCharacters.BoxLightUpHorizontal),
		(true, true, false, false) → Code(SpecialCharacters.BoxLightVertical),
		(true, true, false, true) → Code(SpecialCharacters.BoxLightVerticalLeft),
		(true, true, true, false) → Code(SpecialCharacters.BoxLightVerticalRight),
		(true, true, true, true) → Code(SpecialCharacters.BoxLightVerticalHorizontal)
	)
	
	private val doubleMap: Map[(Boolean, Boolean, Boolean, Boolean), Code] = Map(
		(false, false, false, false) → Code('O'),
		(false, false, false, true) → Code('O'),
		(false, false, true, false) → Code('O'),
		(false, false, true, true) → Code(SpecialCharacters.BoxDoubleHorizontal),
		(false, true, false, false) → Code('O'),
		(false, true, false, true) → Code(SpecialCharacters.BoxDoubleDownLeft),
		(false, true, true, false) → Code(SpecialCharacters.BoxDoubleDownRight),
		(false, true, true, true) → Code(SpecialCharacters.BoxDoubleDownHorizontal),
		(true, false, false, false) → Code('O'),
		(true, false, false, true) → Code(SpecialCharacters.BoxDoubleUpLeft),
		(true, false, true, false) → Code(SpecialCharacters.BoxDoubleUpRight),
		(true, false, true, true) → Code(SpecialCharacters.BoxDoubleUpHorizontal),
		(true, true, false, false) → Code(SpecialCharacters.BoxDoubleVertical),
		(true, true, false, true) → Code(SpecialCharacters.BoxDoubleVerticalLeft),
		(true, true, true, false) → Code(SpecialCharacters.BoxDoubleVerticalRight),
		(true, true, true, true) → Code(SpecialCharacters.BoxDoubleVerticalHorizontal)
	)
	
	def apply(): BoxSelector = BoxSelector(hasUp = false, hasDown = false, hasLeft = false, hasRight = false)
	
}
